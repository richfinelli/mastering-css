$(function() {
	$(".expand-collapse").on("click", function() {
		$(this).toggleClass("open");
		var collapsable = $(this).parent("h2").siblings(".collapsable");
		collapsable.slideToggle();
	});
	//$("body").addClass("animation-ready");
});
// $(window).load(function() {
// 	$("body").addClass("animation-ready");
// });