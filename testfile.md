## Git Notes

###Branches

git branch myBranchName

git checkout myBranchName

git rebase master = updates your branch with master stuff

git checkout master

git merge myBranchName

###Log 
git log --pretty=oneline

###Squash

merges several commits into one

git merge myBranchName --squash - compresses several commits into one